//Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Как я понимаю, это определеный метод(ф-ция) в котором описывается определенное действие над определенным моментом(действием), типо нажали на button  произошли пять разных действий... и это нызывается обработчиком собитий, как то так.
const input = document.querySelector('.price');
const main_div = document.querySelector('.main_div');
let span = document.createElement('span');
span.style.border = '1px solid rgb(221,221,221)';
span.style.backgroundColor = 'rgb(221,221,221)';
span.style.borderRadius = '13px';
span.style.cursor = 'pointer';
let button = document.createElement('button');
button.style.borderRadius = '13px';
button.style.fontSize = '9px';
button.style.border = '1px';
button.style.Color = 'red';
button.style.cursor = 'pointer';
document.body.prepend(span);
span.innerHTML = `Current price: ${input.value}`;
input.style.color = 'green';
button.innerText = 'x';
span.append(button);
span.style.visibility = 'hidden';
let div_error = document.createElement('div');
div_error.innerText = 'Please enter correct price';
div_error.style.color = 'red';
div_error.style.visibility = 'hidden';
main_div.appendChild(div_error);
input.addEventListener('focus', onEventColorBorder);
input.addEventListener('blur', onBlurColorBorder);
input.setAttribute('type', 'number');

function onEventColorBorder(event) {
    event.target.style.borderColor = 'green';
    // func();
}

function onBlurColorBorder(event) {
    event.target.style.borderColor = '';
    func();
}

span.onclick =
    function () {
        span.style.visibility = 'hidden';
        button.remove();
        div_error.style.visibility = 'hidden';
        input.value = '';
    };

function func() {
    if (input.value <= 0) {
        span.style.visibility = 'hidden';
        input.style.borderColor = "red";
        input.style.color = 'red';
        div_error.innerText = 'Please enter correct price';
        div_error.style.color = 'red';
        div_error.style.visibility = 'visible';
    } else if (input.value > 0) {
        div_error.style.visibility = 'hidden';
        span.style.visibility = 'visible';
        document.body.prepend(span);
        if (span) {
            input.style.borderColor = '';
            span.innerHTML = `Current price: ${input.value}`;
            input.style.color = 'green';
            button.innerText = 'x';
            span.append(button);
        }
    } else if (isNaN(input.value)) {
        button.remove();
        div_error.style.visibility = 'hidden';
        span.style.visibility = 'hidden';
    }
}